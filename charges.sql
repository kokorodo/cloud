INSERT INTO `baas2_dashboard`.`charges` (`id`, `request_price`, `flow_price`, `storage_price`, `engine`, `storage`, `flow`, `request`, `charges`, `subtitle`, `type`, `created_at`, `updated_at`) VALUES
(1, 0.00, 0.000, 0.000, 0.00, 20.00, 2.00, 100000, 3.00, '中级用量配额，适用于创业项目、种子期项目', '个人版（3元/天）', NULL, NULL),
(2, 0.00, 0.000, 0.000, 0.00, 200.00, 20.00, 1000000, 30.00, '高级用量配额，适用于成熟项目', '企业版（30元/天）', NULL, NULL),
(3, 0.50, 0.300, 0.005, 2.00, 0.00, 0.00, 0, 0.00, '自由支配用量配额，灵活运用于所有项目', '标准版', NULL, NULL);
