# 素材管理

------

#### 获取临时素材

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| media_id | 是 | 媒体文件ID |

```js
const getMedia = await wxa.getMedia( mediaId );
```

#### 新增临时素材

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| filepath | 是 | 图片文件路径 |

```js
const uploadImage = await wxa.uploadImage( filepath );
```