﻿//在线客服
$(function() {
    $(window).scroll(function() {
        if ($(window).scrollTop() > 800) {
            $(".image_lqVcYO").fadeIn(500);
        } else {
            $(".sliderbar-up").fadeOut(500);
        }
    });

    $(".sliderbar-qq:eq(0)").click(function() {
        window.open(
            "http://wpa.qq.com/msgrd?v=3&uin=2849907180&site=qq&menu=yes"
        );
        return false;
    });

    $(".sliderbar-qq:eq(1)").click(function() {
        window.open(
            "http://wpa.qq.com/msgrd?v=3&uin=1642709442&site=qq&menu=yes"
        );
        return false;
    });

    $(".sliderbar-up").click(function() {
        $("body, html").animate({ scrollTop: 0 }, 1000);
        return false;
    });

    $(".sliderbar-up").mouseover(function() {
        $(".sliderbar-up .sliderbar-item").addClass("active");

        $(".sliderbar-up-show").show();
    });

    $(".sliderbar-up").mouseout(function() {
        $(".sliderbar-up .sliderbar-item").removeClass("active");

        $(".sliderbar-up-show").hide();
    });

    $(".sliderbar-qq").mouseover(function() {
        $(this).find(".sliderbar-item").addClass("active");

        $(".sliderbar-qq-show").show();
    });

    $(".sliderbar-qq").mouseout(function() {
        $(".sliderbar-qq .sliderbar-item").removeClass("active");

        $(".sliderbar-qq-show").hide();
    });

    $(".sliderbar-phone").mouseover(function() {
        $(".sliderbar-phone .sliderbar-item").addClass("active");

        $(".sliderbar-phone-show").show();
    });

    $(".sliderbar-phone").mouseout(function() {
        $(".sliderbar-phone .sliderbar-item").removeClass("active");

        $(".sliderbar-phone-show").hide();
    });

    $(".sliderbar-qrcode").mouseover(function() {
        $(".sliderbar-qrcode .sliderbar-item").addClass("active");

        $(".sliderbar-qrcode-show").show();
    });

    $(".sliderbar-qrcode").mouseout(function() {
        $(".sliderbar-qrcode .sliderbar-item").removeClass("active");

        $(".sliderbar-qrcode-show").hide();
    });
});

//返回顶部
$(function() {
    $(window).bind(
        "scroll",
        {
            fixedOffsetBottom: parseInt($("#Fixed").css("bottom"))
        },
        function(e) {
            var scrollTop = $(window).scrollTop();
            var referFooter = $("#newBottomHtml");
            scrollTop > 100 ? $("#goTop").show() : $("#goTop").hide();
            if (!/msie 6/i.test(navigator.userAgent)) {
                if (
                    $(window).height() -
                        (referFooter.offset() - $(window).scrollTop()) >
                    e.data.fixedOffsetBottom
                ) {
                    $("#Fixed").css(
                        "bottom",
                        $(window).height() -
                            (referFooter.offset() - $(window).scrollTop())
                    );
                } else {
                    $("#Fixed").css("bottom", e.data.fixedOffsetBottom);
                }
            }
        }
    );
    $("#goTop").click(function() {
        $("body,html").stop().animate({
            scrollTop: 0,
            duration: 100,
            easing: "ease-in"
        });
    });
});

//滚动
$(function() {
    $(window).scroll(function() {
        var top = $(window).scrollTop();
        var offset = $(".image_lqVcYO").offset();
        var offset1 = $("#imgs1").offset();
        var offset2 = $("#imgs2").offset();
        var offset3 = $("#imgs3").offset();
        var height = 800 - Math.abs(offset.top - top);
        var height1 = 900 - Math.abs(offset1.top - top);
        var height2 = 900 - Math.abs(offset2.top - top);
        var height3 = 900 - Math.abs(offset3.top - top);
        // console.log(offset.top, top, height);
        if (height >= 0) {
            var r = offset.top,
                o = (top - r + 400) / 10;
            if (o > 40) {
                o = 40;
            }
            $(".image_lqVcYO").css({
                transform: "translate3d(0px, -" + o + "px, 0px)"
            });
        }
        if (height1 >= 0) {
            var r = offset1.top,
                o = (top - r + 400) / 10;
            if (o > 30) {
                o = 30;
            }
            $("#imgs1").css({
                transform: "translate3d(0px, -" + o + "px, 0px)"
            });
        }
        if (height2 >= 0) {
            var r = offset2.top,
                o = (top - r + 400) / 10;
            if (o > 30) {
                o = 30;
            }
            $("#imgs2").css({
                transform: "translate3d(0px, -" + o + "px, 0px)"
            });
        }
        if (height3 >= 0) {
            var r = offset3.top,
                o = (top - r + 400) / 10;
            if (o > 30) {
                o = 30;
            }
            $("#imgs3").css({
                transform: "translate3d(0px, -" + o + "px, 0px)"
            });
        }
    });
});

//导航点击
$(function() {
    $(".header-nav").click(function() {
        $(".header-center").css({
            display: "none"
        });
        $(".moblie-menu-layout").show();
        return false;
    });
    $(".header-navs").click(function() {
        $(".moblie-menu-layout").css({
            display: "none"
        });
        $(".header-center").show();
        return false;
    });
});
