-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2020-03-02 10:40:28
-- 服务器版本： 5.7.21-log
-- PHP 版本： 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `baas2`
--
CREATE DATABASE IF NOT EXISTS `baas2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `baas2`;

-- --------------------------------------------------------

--
-- 表的结构 `auth`
--

CREATE TABLE `auth` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `secret` text,
  `expires` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `baas`
--

CREATE TABLE `baas` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` text,
  `appid` text,
  `appkey` text,
  `database` text,
  `connection` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `private` int(11) NOT NULL DEFAULT '0' COMMENT '私有',
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class`
--

CREATE TABLE `class` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class_auth`
--

CREATE TABLE `class_auth` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `auth_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class_field`
--

CREATE TABLE `class_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `name` text,
  `value` text,
  `type` text,
  `required` int(11) NOT NULL DEFAULT '0' COMMENT '客户端是否需要传值',
  `url` text COMMENT '过滤URL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class_function`
--

CREATE TABLE `class_function` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `body` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class_hook`
--

CREATE TABLE `class_hook` (
  `id` int(10) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `body` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class_schedule`
--

CREATE TABLE `class_schedule` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `function_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `rule` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `class_table`
--

CREATE TABLE `class_table` (
  `id` int(10) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `fetch` int(11) NOT NULL DEFAULT '1',
  `add` int(11) NOT NULL DEFAULT '0',
  `update` int(11) NOT NULL DEFAULT '0',
  `delete` int(11) NOT NULL DEFAULT '0',
  `auth_id` int(11) NOT NULL DEFAULT '0' COMMENT '输出结果加密ID，0未加密',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `file`
--

CREATE TABLE `file` (
  `id` int(10) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `ext` text,
  `type` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `savename` text,
  `savepath` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `global`
--

CREATE TABLE `global` (
  `id` int(10) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `body` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `relation`
--

CREATE TABLE `relation` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `target` text,
  `targeted` text COMMENT '关联昵称，默认target',
  `relation` text,
  `foreign_key` text,
  `foreign_key_target` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `table`
--

CREATE TABLE `table` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `uuid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自增主键uuid',
  `hidden` text,
  `unique` text COMMENT '唯一值',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`);

--
-- 表的索引 `baas`
--
ALTER TABLE `baas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appid` (`appid`(12)),
  ADD KEY `appkey` (`appkey`(42));

--
-- 表的索引 `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`);

--
-- 表的索引 `class_auth`
--
ALTER TABLE `class_auth`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `auth_id` (`auth_id`);

--
-- 表的索引 `class_field`
--
ALTER TABLE `class_field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`),
  ADD KEY `class_id` (`class_id`);

--
-- 表的索引 `class_function`
--
ALTER TABLE `class_function`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`),
  ADD KEY `class_id` (`class_id`);

--
-- 表的索引 `class_hook`
--
ALTER TABLE `class_hook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`),
  ADD KEY `class_id` (`class_id`);

--
-- 表的索引 `class_schedule`
--
ALTER TABLE `class_schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `function_id` (`function_id`);

--
-- 表的索引 `class_table`
--
ALTER TABLE `class_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`),
  ADD KEY `class_id` (`class_id`);

--
-- 表的索引 `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`);

--
-- 表的索引 `global`
--
ALTER TABLE `global`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`);

--
-- 表的索引 `relation`
--
ALTER TABLE `relation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`);

--
-- 表的索引 `table`
--
ALTER TABLE `table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `baas_id` (`baas_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `baas`
--
ALTER TABLE `baas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class_auth`
--
ALTER TABLE `class_auth`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class_field`
--
ALTER TABLE `class_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class_function`
--
ALTER TABLE `class_function`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class_hook`
--
ALTER TABLE `class_hook`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class_schedule`
--
ALTER TABLE `class_schedule`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `class_table`
--
ALTER TABLE `class_table`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `file`
--
ALTER TABLE `file`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `global`
--
ALTER TABLE `global`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `relation`
--
ALTER TABLE `relation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `table`
--
ALTER TABLE `table`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 数据库： `baas2_analysis`
--
CREATE DATABASE IF NOT EXISTS `baas2_analysis` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `baas2_analysis`;

-- --------------------------------------------------------

--
-- 表的结构 `cost_log`
--

CREATE TABLE `cost_log` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `orderid` text COMMENT '单号',
  `money` double(10,4) NOT NULL DEFAULT '0.0000' COMMENT '消费金额',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1消费。2充值',
  `status` int(2) DEFAULT NULL COMMENT '0未，1已',
  `remark` text COMMENT '备注',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `statistics`
--

CREATE TABLE `statistics` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `request_num` int(11) NOT NULL DEFAULT '0' COMMENT '一天请求数',
  `storage` text COMMENT '当天存储',
  `debug` int(11) NOT NULL DEFAULT '0' COMMENT '调试',
  `flow` text COMMENT '流量',
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `cost_log`
--
ALTER TABLE `cost_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `cost_log`
--
ALTER TABLE `cost_log`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 数据库： `baas2_dashboard`
--
CREATE DATABASE IF NOT EXISTS `baas2_dashboard` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `baas2_dashboard`;

-- --------------------------------------------------------

--
-- 表的结构 `baas_privilege`
--

CREATE TABLE `baas_privilege` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `privilege_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `charges`
--

CREATE TABLE `charges` (
  `id` int(11) UNSIGNED NOT NULL,
  `request_price` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '次/天',
  `flow_price` double(10,3) NOT NULL DEFAULT '0.000' COMMENT 'GB/天',
  `storage_price` double(10,3) NOT NULL DEFAULT '0.000' COMMENT 'GB/天',
  `engine` double(10,2) NOT NULL COMMENT '天',
  `storage` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '套餐，内存额度',
  `flow` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '套餐，流量额度',
  `request` int(10) NOT NULL DEFAULT '0' COMMENT '套餐，请求次数',
  `charges` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '套餐费用',
  `subtitle` text COMMENT '副标题',
  `type` text COMMENT 'standard标准版，person个人版，company公司',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `company`
--

CREATE TABLE `company` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `company_name` text COMMENT '企业名称',
  `credit_num` text COMMENT '企业信用代码',
  `license_id` int(11) NOT NULL DEFAULT '0' COMMENT '执照图片id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) UNSIGNED NOT NULL,
  `price` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '代金券金额',
  `name` text COMMENT '代金券名称',
  `time` int(11) DEFAULT NULL COMMENT '时长',
  `time_type` text COMMENT '时间类型',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `domain`
--

CREATE TABLE `domain` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text COMMENT '域名',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `email`
--

CREATE TABLE `email` (
  `id` int(11) UNSIGNED NOT NULL,
  `service` text COMMENT '邮箱类型',
  `user` text COMMENT '邮箱账号',
  `pass` text COMMENT '邮箱发送密码',
  `from` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '发送者',
  `subject` text COMMENT '标题',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `invite_user`
--

CREATE TABLE `invite_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `invited_id` int(11) NOT NULL DEFAULT '0' COMMENT '推荐过来的人',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `notice`
--

CREATE TABLE `notice` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` text COMMENT '消息标题',
  `content` text COMMENT '消息内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `privilege`
--

CREATE TABLE `privilege` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` text COMMENT '特权名称',
  `price` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '特权价格',
  `remark` text COMMENT '简介',
  `icon` text COMMENT '特权图标',
  `detail` text COMMENT '特权介绍',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `recharge`
--

CREATE TABLE `recharge` (
  `id` int(11) UNSIGNED NOT NULL,
  `price` double(10,2) NOT NULL DEFAULT '0.00',
  `discount_price` double(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `reward`
--

CREATE TABLE `reward` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `coupon_price` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '未到账优惠券金额',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `trade`
--

CREATE TABLE `trade` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `tradeid` text,
  `money` float(10,2) NOT NULL DEFAULT '0.00',
  `payment` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0: 未支付，1: 支付成功，-1: 退款成功',
  `notify_url` text,
  `notify_data` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `phone` text COMMENT '手机号',
  `password` text COMMENT '登录密码',
  `password_org` text COMMENT '未加密登录密码',
  `lastip` text COMMENT '最后登录ip',
  `money` float(10,4) NOT NULL DEFAULT '0.0000' COMMENT '账户余额',
  `login_date` date DEFAULT NULL COMMENT '最近登录时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 触发器 `user`
--
DELIMITER $$
CREATE TRIGGER `add_user_type` AFTER INSERT ON `user` FOR EACH ROW BEGIN
insert into user_type(user_id,created_at) values(new.id,new.created_at);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- 表的结构 `user_baas`
--

CREATE TABLE `user_baas` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `baas_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `charges_id` int(11) UNSIGNED NOT NULL DEFAULT '3' COMMENT '收费方式id',
  `engine` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 触发器 `user_baas`
--
DELIMITER $$
CREATE TRIGGER `log_count_add` AFTER INSERT ON `user_baas` FOR EACH ROW BEGIN
insert into baas2_analysis.statistics(baas_id,flow,storage,created_at) values(new.baas_id,'0B','0B',new.created_at);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `sms_add` BEFORE INSERT ON `user_baas` FOR EACH ROW BEGIN
insert into baas2_plugin.sms(baas_id,created_at) values(new.baas_id,new.created_at);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- 表的结构 `user_coupon`
--

CREATE TABLE `user_coupon` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `coupon_id` int(11) NOT NULL DEFAULT '0' COMMENT '优惠券id',
  `ended_at` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '-1已过期，0未兑换，1已兑换',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_image`
--

CREATE TABLE `user_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `ext` text,
  `type` text,
  `savename` text,
  `savepath` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `nickname` text COMMENT '昵称',
  `qq_num` text COMMENT 'qq号',
  `avater_id` int(11) NOT NULL DEFAULT '0' COMMENT '头像图片id',
  `sign` text COMMENT '签名',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_notice`
--

CREATE TABLE `user_notice` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `notice_id` int(11) NOT NULL DEFAULT '0' COMMENT '消息id',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未读，1已读',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0:个人,1企业',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `verify`
--

CREATE TABLE `verify` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` text,
  `value` text,
  `ip` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:未使用，1:已核销',
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `baas_privilege`
--
ALTER TABLE `baas_privilege`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `charges`
--
ALTER TABLE `charges`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `domain`
--
ALTER TABLE `domain`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `invite_user`
--
ALTER TABLE `invite_user`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `recharge`
--
ALTER TABLE `recharge`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `reward`
--
ALTER TABLE `reward`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `trade`
--
ALTER TABLE `trade`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_baas`
--
ALTER TABLE `user_baas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `charges_id` (`charges_id`);

--
-- 表的索引 `user_coupon`
--
ALTER TABLE `user_coupon`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_image`
--
ALTER TABLE `user_image`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_notice`
--
ALTER TABLE `user_notice`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `verify`
--
ALTER TABLE `verify`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `baas_privilege`
--
ALTER TABLE `baas_privilege`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `charges`
--
ALTER TABLE `charges`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `domain`
--
ALTER TABLE `domain`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `invite_user`
--
ALTER TABLE `invite_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `privilege`
--
ALTER TABLE `privilege`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `recharge`
--
ALTER TABLE `recharge`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `reward`
--
ALTER TABLE `reward`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `trade`
--
ALTER TABLE `trade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_baas`
--
ALTER TABLE `user_baas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_coupon`
--
ALTER TABLE `user_coupon`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_image`
--
ALTER TABLE `user_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_notice`
--
ALTER TABLE `user_notice`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `verify`
--
ALTER TABLE `verify`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 限制导出的表
--

--
-- 限制表 `user_baas`
--
ALTER TABLE `user_baas`
  ADD CONSTRAINT `user_baas_ibfk_1` FOREIGN KEY (`charges_id`) REFERENCES `charges` (`id`);
--
-- 数据库： `baas2_log`
--
CREATE DATABASE IF NOT EXISTS `baas2_log` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `baas2_log`;

-- --------------------------------------------------------

--
-- 表的结构 `debug`
--

CREATE TABLE `debug` (
  `id` int(10) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` text,
  `message` text,
  `info` text,
  `device` text,
  `system` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `method` text,
  `url` text,
  `status` int(11) NOT NULL DEFAULT '200',
  `time` text,
  `length` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sms`
--

CREATE TABLE `sms` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `phone` text COMMENT '手机号',
  `content` text COMMENT '内容',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态0失败1成功',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `debug`
--
ALTER TABLE `debug`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `debug`
--
ALTER TABLE `debug`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 数据库： `baas2_plugin`
--
CREATE DATABASE IF NOT EXISTS `baas2_plugin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `baas2_plugin`;

-- --------------------------------------------------------

--
-- 表的结构 `captcha_img`
--

CREATE TABLE `captcha_img` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(10) DEFAULT NULL,
  `code` text COMMENT '验证码模版code',
  `length` int(10) NOT NULL DEFAULT '0' COMMENT '验证码长度',
  `exclude` text NOT NULL COMMENT '字符排除',
  `line` int(10) NOT NULL DEFAULT '0' COMMENT '干扰线条数量',
  `color` tinyint(2) NOT NULL DEFAULT '0' COMMENT '字符是否有颜色',
  `bgcolor` text COMMENT '验证码图片背景色',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `captcha_sms`
--

CREATE TABLE `captcha_sms` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(10) NOT NULL DEFAULT '0',
  `name` text COMMENT '模板名称',
  `content` text COMMENT '模板内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `mail`
--

CREATE TABLE `mail` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `host` text COMMENT '主机',
  `secureConnection` text COMMENT '使用 SSL',
  `port` int(11) NOT NULL DEFAULT '0' COMMENT 'SMTP 端口',
  `user` text COMMENT '邮箱登录账号',
  `pass` text COMMENT '使用QQ邮箱登录密码是不正确的，必须使用QQ邮箱里的授权码，这个请注意，底部有说明。',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `pay_alipay`
--

CREATE TABLE `pay_alipay` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `account` text COMMENT '账户号',
  `partner` text COMMENT '合作者身份ID',
  `key` text COMMENT '密钥',
  `class_id` int(11) NOT NULL DEFAULT '0' COMMENT '函数分组id',
  `function_id` int(11) NOT NULL DEFAULT '0' COMMENT '函数id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `pay_weixin`
--

CREATE TABLE `pay_weixin` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `appid` text COMMENT '公众号appid',
  `mchid` text COMMENT '商户号',
  `key` text COMMENT '密钥',
  `class_id` int(11) NOT NULL DEFAULT '0' COMMENT '分组id',
  `function_id` int(11) NOT NULL DEFAULT '0' COMMENT '函数id',
  `certificate` text COMMENT '证书',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `pay_wxa`
--

CREATE TABLE `pay_wxa` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `appid` text COMMENT '小程序appid',
  `mchid` text COMMENT '商户号',
  `key` text COMMENT '密钥',
  `class_id` int(11) NOT NULL DEFAULT '0' COMMENT '分组id',
  `function_id` int(11) NOT NULL DEFAULT '0' COMMENT '函数id',
  `certificate` text COMMENT '证书',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sms`
--

CREATE TABLE `sms` (
  `id` int(11) UNSIGNED NOT NULL,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '短信条数',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sms_fee`
--

CREATE TABLE `sms_fee` (
  `id` int(11) UNSIGNED NOT NULL,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转储表的索引
--

--
-- 表的索引 `captcha_img`
--
ALTER TABLE `captcha_img`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `captcha_sms`
--
ALTER TABLE `captcha_sms`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `pay_alipay`
--
ALTER TABLE `pay_alipay`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `pay_weixin`
--
ALTER TABLE `pay_weixin`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `pay_wxa`
--
ALTER TABLE `pay_wxa`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sms_fee`
--
ALTER TABLE `sms_fee`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `captcha_img`
--
ALTER TABLE `captcha_img`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `captcha_sms`
--
ALTER TABLE `captcha_sms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `pay_alipay`
--
ALTER TABLE `pay_alipay`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `pay_weixin`
--
ALTER TABLE `pay_weixin`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `pay_wxa`
--
ALTER TABLE `pay_wxa`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sms_fee`
--
ALTER TABLE `sms_fee`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
